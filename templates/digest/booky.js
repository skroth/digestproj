(function() {
    function getElementIndex(e) {
        var i = 0,
            prev = e.previousElementSibling
        while (prev) {
            i++
            prev = prev.previousElementSibling
        }

        return i
    }

    function getElementXPath(element) {
        var parent = element.parentElement,
            hierachy = {
                child: null,
                tag: element.tagName,
                id: element.id,
                classes: element.className.split(' ')
                    .filter(function(e) { return !!e }),
                index: getElementIndex(element)
            }

        while (parent.parentElement) {
            var tmp = hierachy
            hierachy = {
                child: tmp,
                tag: parent.tagName,
                id: parent.id,
                classes: parent.className.split(' ')
                    .filter(function(e) { return !!e }),
                index: getElementIndex(parent)
            }

            parent = parent.parentElement
        }

        return hierachy
    }

    // the minimum version of jQuery we want
    var v = "1.9.1";

    // code from http://coding.smashingmagazine.com/2010/05/23/make-your-own-bookmarklets-with-jquery/
    // also thanks to http://betterexplained.com/articles/how-to-make-a-bookmarklet-for-your-web-application/
    if (window.jQuery === undefined || window.jQuery.fn.jquery < v) {
        var done = false;
        var script = document.createElement("script");
        script.src = "http://ajax.googleapis.com/ajax/libs/jquery/" + v + "/jquery.min.js";
        script.onload = script.onreadystatechange = function(){
            if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
                done = true;
                initMyBookmarklet();
            }
        };
        document.getElementsByTagName("head")[0].appendChild(script);
    } else {
        initMyBookmarklet();
    }

    function initMyBookmarklet() {
        (window.myBookmarklet = function() {
            var greatestZIndex = 2;

            $('<style>').html('a:hover{background-color:red}.comeuppance{background-color:red}#digest_button{bottom:5px;background-color:white;border:1px solid black;cursor:pointer;font-size:14pt;font-family:sans-serif;position:fixed;padding:3px;right:5px;z-index:2} .digest_popup{z-index:3; position:absolute; width:100px; margin-left:-50px; height 20px; background-color:white; border:1px solid grey; border-radius: 3px; text-align:center; cursor:pointer; font-size: 12px;} .digest_related{background-color:yellow;}').appendTo(document.body);
            $('<div>').text('Digest It!')
                .attr('id', 'digest_button')
                .click(function() {
                    var links = []
                    elements.each(function() {
                        if($(this).data('dijest')) {
                            links.push($(this).data('deets'));
                        }
                    });
                    //console.log(links);
                    $.post('http://{{ host }}/subscribe/', {'url':document.location.href, 'links':JSON.stringify(links),
                        'cont':document.getElementsByTagName('html')[0].innerHTML}, function(data){
                        window.location = 'http://{{ host }}/edit_feed/'+data+'/';
                        console.log('we good');
                    }, 'text');
                })
                .appendTo(document.body)
                .siblings().each(function (i,e) {
                    var z = parseInt($(e).css('z-index'));
                    if (!!z && z > greatestZIndex) greatestZIndex = z;
                }).end()
                .css('z-index', greatestZIndex+1);

            var elements = $("a");
            elements.click(function(e) {
            // Begin Sean code ************************************************
                var elms = [];
                var $this = $(this);
                var done = false;
                if($this.attr('id')) {
                    elms.unshift('//'+$this.prop('tagName').toLowerCase()+'[@id="'+$this.attr('id')+'"]');
                    done = true;
                } else {
                    elms.unshift('/'+$this.prop('tagName').toLowerCase());
                }
                var parents = $(this).parents().each(function() {
                    if(done) return;
                    var $this = $(this);

                    if($this.attr('id')) {
                        elms.unshift('//'+$this.prop('tagName').toLowerCase()+'[@id="'+$this.attr('id')+'"]');
                        done = true;
                    } else {
                        elms.unshift('/'+($this.prop('tagName').toLowerCase()));
                    }

                });
                var link = {"href":$this.attr('href'),
                            "class":$this.attr('class'),
                            "txt":$this.text(),
                            "xpath":elms.join('')};
                //console.log(link);
                //var xpath = elms.join('');
                //console.log(xpath);
                //console.log($(xpath).text());

                //console.log(getElementXPath($this));
                // End sean code ********************************************************
                // Begin ryan code *****************************************************

                if ($this.hasClass('comeuppance')) {
                    $this.data('dijest', false);
                    $this.removeClass('comeuppance');
                } else {
                    $this.data('dijest', true);
                    $this.data('deets', link)
                    $this.addClass('comeuppance');
                }
                // Testing Code
                //console.log(JSON.stringify(getElementXPath(this)));
                //$.post('http://localhost:8000/subscribe/', {'url':'http://reddit.com/', 'hierarchy':JSON.stringify(getElementXPath(this))})


                var selector = link.xpath.replace(/.+\[@id="(.+?)"\]/, '#$1');
                    selector = selector.replace(/\[.+\]/g, '');
                    selector = selector.replace(/\//g, '>');

                var offset = $this.offset(),
                    // Actually select related links here
                    relatedLinks = $(selector).not('.comeuppance');

                offset.top -= 20;
                offset.left += $this.width()/2;


                $('.digest_popup').remove()
                $('<div>').addClass('digest_popup')
                    .text('Select Related')
                    .css(offset)
                    .hover(function() {
                        relatedLinks.addClass('digest_related');
                    }, function() {
                        relatedLinks.removeClass('digest_related');
                    })
                    .click(function() {
                        relatedLinks.removeClass('digest_related');
                        relatedLinks.click();
                        $('.digest_popup').remove()
                    })
                    .appendTo(document.body);

                e.preventDefault();
            })

        })();
    }

})();