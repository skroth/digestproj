# Digest Proj
## A CSC 697 project by Sean Roth
### With contributions by Ryan Jenkins
This project allows a user to turn a webpage into a periodic email digest -- regardless of whether or not the page has an RSS feed.
## What to look for
If you haven't used django, the main parts are in `digest/models.py` where the db schema lives, `digest/views.py` where each view is, `digest/tests.py` where the tests live, and `digest/management/commands/checkfeeds.py` where the code that checks the feeds lives.
`booky.js` in the templates folder is an important file. It holds the Javascript code that is run when the bookmarklet is used.
