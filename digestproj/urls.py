from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin

admin.autodiscover()
urlpatterns = patterns('',
    (r'^$', 'digest.views.index'),
    (r'^newfeed/$', TemplateView.as_view(template_name='digest/setup.html')),
    (r'^edit_feed/(?P<fid>\d+)/(?P<token>[a-zA-Z]{49})/', 'digest.views.edit_feed'),
    (r'^register/$', 'digest.views.register'),
    (r'^passthrough/$', 'digest.views.passthrough'),
    (r'^subscribe/$', 'digest.views.handle_subscription'),
    (r'^booky.js/$', 'digest.views.booky'),
    # Examples:
    # url(r'^$', 'digestproj2.views.home', name='home'),
    # url(r'^digestproj2/', include('digestproj2.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
