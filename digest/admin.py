__author__ = 'sean'
from django.contrib import admin
from models import Feed, FeedItem

class FeedAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    list_display = ('title','url','created','last_crawled','next_email','get_interval_days', 'email', 'max_amount', 'unique_token','enabled')
    ordering = ('-created',)
    list_filter = ('enabled',)
    search_fields = ('title','url','xpaths','email')

class FeedItemAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    list_display = ('title','url','created','xpath','score','last_active')
    ordering = ('-created',)
    search_fields = ('title','url','xpath')

admin.site.register(Feed, FeedAdmin)
admin.site.register(FeedItem, FeedItemAdmin)