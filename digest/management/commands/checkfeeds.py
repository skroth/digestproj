__author__ = 'Sean Roth'

import urllib2
from datetime import datetime, timedelta
from hashlib import md5
from lxml.html import fromstring
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from digest.models import Feed, FeedItem

class Command(BaseCommand):

    def handle(self, *args, **options):

        # Crawl all feeds that are enabled and have xpaths and emails
        for feed in Feed.objects.filter(enabled=True).exclude(xpaths='').exclude(email='').select_related():
            xpaths = feed.xpaths.split(',|&')
            parsed = feed.get_parsed()
            parsed.make_links_absolute(base_url=feed.url)
            for xpath in xpaths:
                try:
                    el = parsed.xpath(xpath)[0]
                except IndexError:
                    print "Could not find el at xpath: ",
                    print xpath
                    #print parsed
                    #print tostring(parsed)
                    print feed.url
                    continue
                try:
                    item = feed.feeditem_set.get(url=el.attrib.get('href'))
                except FeedItem.DoesNotExist:
                    title = el.text if el.text else ''
                    new_item = FeedItem(title=title.strip(),
                                        url=el.attrib.get('href',''),
                                        feed=feed,)
                    new_item.save()
                else:
                    item.score += 1
                    item.last_active = datetime.now()
                    item.save()
            feed.last_crawled = datetime.now()
            feed.save()

        # Send emails that are due or should be due in 5 minutes
        for feed in Feed.objects.filter(next_email__lte=datetime.now()+timedelta(minutes=5),enabled=True)\
                .exclude(email='').select_related():
            feed.refresh_token()
            body = ["Hi there,\n\nHere's what's new on %s:\n\n" % feed.url]
            i = 1
            for item in feed.feeditem_set.filter(last_active__gte=datetime.now()-timedelta(seconds=feed.email_interval)).order_by('-score'):
                if i > feed.max_amount:
                    break
                body.append("%d. %s [%s]" % (i, item.title, item.url))
                i += 1
            body.extend(["\n\nTo modify or delete this digest, visit this link: http://digest.seanroth.com%s\n" %
                        feed.get_absolute_url(),
                        "Please note that this link changes every in every email, so you must always use the link from the latest one.\n",
                        "Do not reply to this message. Feel free to contact me at seanroth [at] outlook [dot] com :)"])


            feed.next_email = datetime.now() + timedelta(seconds=feed.email_interval)
            feed.save()
            send_mail('Your digest: %s' % feed.title, '\n'.join(body), 'sean@seanroth.com',
                      [feed.email], fail_silently=False)
