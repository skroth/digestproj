# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'FeedItem.xpath'
        db.alter_column(u'digest_feeditem', 'xpath', self.gf('django.db.models.fields.TextField')())

        # Changing field 'FeedItem.title'
        db.alter_column(u'digest_feeditem', 'title', self.gf('django.db.models.fields.TextField')())

        # Changing field 'FeedItem.url'
        db.alter_column(u'digest_feeditem', 'url', self.gf('django.db.models.fields.URLField')(max_length=500))

        # Changing field 'Feed.title'
        db.alter_column(u'digest_feed', 'title', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):

        # Changing field 'FeedItem.xpath'
        db.alter_column(u'digest_feeditem', 'xpath', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'FeedItem.title'
        db.alter_column(u'digest_feeditem', 'title', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'FeedItem.url'
        db.alter_column(u'digest_feeditem', 'url', self.gf('django.db.models.fields.URLField')(max_length=255))

        # Changing field 'Feed.title'
        db.alter_column(u'digest_feed', 'title', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'digest.feed': {
            'Meta': {'object_name': 'Feed'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'email_interval': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_crawled': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'max_amount': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'next_email': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 5, 22, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'unique_token': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'xpaths': ('django.db.models.fields.TextField', [], {})
        },
        u'digest.feeditem': {
            'Meta': {'object_name': 'FeedItem'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'feed': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['digest.Feed']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_active': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 5, 21, 0, 0)'}),
            'score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '500'}),
            'xpath': ('django.db.models.fields.TextField', [], {})
        },
        u'digest.feeduser': {
            'Meta': {'object_name': 'FeedUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_email_sent': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'verification_token': ('django.db.models.fields.CharField', [], {'default': "'dtrspsafbwfkdizsyqcvrmgyersnncmqxqoxevjxqrscmbhuq'", 'max_length': '50'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['digest']