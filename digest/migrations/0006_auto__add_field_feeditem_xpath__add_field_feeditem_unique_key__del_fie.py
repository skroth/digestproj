# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'FeedItem.xpath'
        db.add_column(u'digest_feeditem', 'xpath',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Adding field 'FeedItem.unique_key'
        db.add_column(u'digest_feeditem', 'unique_key',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50),
                      keep_default=False)

        # Deleting field 'Feed.owner'
        db.delete_column(u'digest_feed', 'owner_id')

        # Adding field 'Feed.xpaths'
        db.add_column(u'digest_feed', 'xpaths',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'Feed.unique_token'
        db.add_column(u'digest_feed', 'unique_token',
                      self.gf('django.db.models.fields.CharField')(default='dccqutxzfzvtkwqnbhizivaiqlfwxzchcpjcixsbjjfpmshhq', max_length=50),
                      keep_default=False)


        # Changing field 'Feed.last_crawled'
        db.alter_column(u'digest_feed', 'last_crawled', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True))

    def backwards(self, orm):
        # Deleting field 'FeedItem.xpath'
        db.delete_column(u'digest_feeditem', 'xpath')

        # Deleting field 'FeedItem.unique_key'
        db.delete_column(u'digest_feeditem', 'unique_key')


        # User chose to not deal with backwards NULL issues for 'Feed.owner'
        raise RuntimeError("Cannot reverse this migration. 'Feed.owner' and its values cannot be restored.")
        # Deleting field 'Feed.xpaths'
        db.delete_column(u'digest_feed', 'xpaths')

        # Deleting field 'Feed.unique_token'
        db.delete_column(u'digest_feed', 'unique_token')


        # Changing field 'Feed.last_crawled'
        db.alter_column(u'digest_feed', 'last_crawled', self.gf('django.db.models.fields.DateTimeField')(null=True))

    models = {
        u'digest.feed': {
            'Meta': {'object_name': 'Feed'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email_interval': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_crawled': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'next_email': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'unique_token': ('django.db.models.fields.CharField', [], {'default': "'cdlodckshoxpwvtjsvduikbxbjozgtkwaiwiwhweqssebfibr'", 'max_length': '50'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'xpaths': ('django.db.models.fields.TextField', [], {})
        },
        u'digest.feeditem': {
            'Meta': {'object_name': 'FeedItem'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'feed': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['digest.Feed']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'score': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'unique_key': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'xpath': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'digest.feeduser': {
            'Meta': {'object_name': 'FeedUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_email_sent': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'verification_token': ('django.db.models.fields.CharField', [], {'default': "'fzxtrsfzinlwcoyrdswdrutaspchtbycmtvvbhsxtjzjileln'", 'max_length': '50'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['digest']