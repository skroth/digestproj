import urllib2, re, json
from hashlib import md5
from urlparse import urljoin
from lxml.html import fromstring, tostring
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt

from models import FeedUser, Feed, FeedItem
from forms import EditFeedForm

def linkrepl(match):
    relative_url = match.group(1)
    return '"'

def index(request):
    return render(request, 'digest/index.html', {'host':request.get_host()})

def booky(request):
    response = render(request, 'digest/booky.js', {'host':request.get_host()})
    response['Access-Control-Allow-Origin'] = '*'
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    return response


def passthrough(request):
    url = request.GET['url']
    response = urllib2.urlopen(url)
    html = response.read()
    response.close()

    doc = fromstring(html)
    doc.make_links_absolute(url)
    return HttpResponse(tostring(doc))
    # Replace relative urls as best we can
    base_url = '/'.join(url.split('/')[:3])  # for links that use /something.gif
    base_url_relative = '/'.join(url.split('/')[:-1])  # for links that use something.gif
    print base_url_relative
    #rel_links = re.findall(r"(?:href|src)\s?=(['\"])((?!http).*?)\1", html)
    replaced = re.sub(r"(href|src)\s?=\s?(?P<quote>['\"])?([0-9a-zA-Z$-_.+!*(),]+)(?P=quote)",
                      "\1=\"\2\"", html)
    replaced = re.sub(r"(?P<first>(?:href|src)\s?(?P<quote>['\"]=|=))(?P<last>/((?!http).+?)(?P=quote))",
                      r"\g<first>%s\g<last>" % base_url, replaced)
    replaced = re.sub(r"(?P<first>(?:href|src)\s?(?P<quote>['\"]=|=))(?P<last>((?!http).+?)(?P=quote))",
                      r"\g<first>"+base_url_relative+r"/\g<last>", replaced)
    #linkrepl = lambda m: '"%s/%s"' % (base_url, m[1])

    return HttpResponse(replaced)

@csrf_exempt
def handle_subscription(request):
    #hierarchy = json.loads(request.POST['hierarchy'])
    #response = urllib2.urlopen(request.POST['url'])
    try:
        link_objs = json.loads(request.POST['links'])
    except ValueError:
        print request.POST['links']
        raise
    content = request.POST['cont']
    url = request.POST['url']  # TODO: Use form for this. This makes me feel dirty.

    parsed = fromstring(content)
    parsed.make_links_absolute(base_url=url)
    new_f = Feed(url=url, title=parsed.find(".//title").text)  # TODO: See above. :'(
    parsed_ours = new_f.get_parsed()
    parsed_ours.make_links_absolute(base_url=url)
    new_f.save()
    xpaths = []
    for link_obj in link_objs:
        link_obj['href'] = urljoin(url, link_obj['href'])
        feed_item = FeedItem(title=link_obj['txt'].strip(),
                             url=link_obj['href'],
                             score=0,
                             feed=new_f)
        elements = parsed.xpath(link_obj['xpath'])
        element = None
        for el in elements:
            if 'comeuppance' in el.attrib.get('class','') and link_obj.get('class','') in el.attrib.get('class','') \
                    and link_obj['txt'] == el.text and link_obj['href'] == el.attrib.get('href'):
                element = el
                break
        if element is None:
            print "returning early"
            return HttpResponse('ERROR', content_type='text/plain')

        feed_item.xpath = element.getroottree().getpath(element)  # Real xpath
        our_els = parsed_ours.xpath(feed_item.xpath)
        if not our_els:  # Means they have a different structure for browsers and spiders
            our_els = parsed_ours.find(".//a[@href='%s']" % link_obj['href'])
            if our_els:
                feed_item.xpath = our_els.getroottree().getpath(our_els)
            else:
                feed_item.xpath = ''
        if feed_item.xpath:
            xpaths.append(feed_item.xpath)
            feed_item.save()
    new_f.xpaths = ',|&'.join(xpaths)  # God help the soul that uses ,|& in an HTML id
    new_f.save()
    response = HttpResponse('%d/%s'%(new_f.id,new_f.unique_token), content_type='text/plain')
    response['Access-Control-Allow-Origin'] = '*'
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    return response


    tree = fromstring(page)
    print tree.getpath()
    print(tree)

    current_heirarchy_node = hierarchy
    current_tree_node = tree

    while current_heirarchy_node:
        candidate_next_nodes = current_tree_node.getchildren()
        current_tree_node = candidate_next_nodes[current_heirarchy_node['index']]
        current_heirarchy_node = current_heirarchy_node['child']
        '''
        candidate_next_nodes = filter(lambda x: x.tag == current_heirarchy_node['tag'])
        if current_heirarchy_node['id']:
            candidate_next_nodes = filter(lambda x: x.tag == current_heirarchy_node['tag'])
        '''

    print current_tree_node

    response = HttpResponse(current_tree_node.text)
    response['Access-Control-Allow-Origin'] = '*'

    return response


def edit_feed(request, fid, token=''):
    fid = int(fid)
    if not Feed.objects.filter(pk=fid, unique_token=token):
        raise Http404
    # If we've gotten this far the user is allowed to edit this feed
    feed = Feed.objects.get(pk=fid)
    if 'delete' in request.GET:
        feed.delete()
        return HttpResponse('DONE', content_type='text/plain')
    if request.method == 'POST':
        form = EditFeedForm(request.POST)
        #form.starting.label = 'Herps'
        if form.is_valid():
            feed.next_email = form.cleaned_data['starting']
            feed.set_interval_days(form.cleaned_data['days_interval'])
            feed.title = form.cleaned_data['title']
            feed.email = form.cleaned_data['email']
            feed.enabled = form.cleaned_data['enabled']
            feed.max_amount = form.cleaned_data['max_amount']
            feed.refresh_token()
            feed.save()
            return redirect('digest.views.edit_feed', fid, feed.unique_token)
    else:
        interval = feed.email_interval/86400 if feed.email_interval else None
        form = EditFeedForm(initial={'url':feed.url, 'title':feed.title, 'email':feed.email,
                                     'starting':feed.next_email,
                                     'days_interval':interval,
                                     'max_amount':feed.max_amount,
                                     'enabled':feed.enabled})

    items = feed.feeditem_set.all()
    return render(request, 'digest/edit_feed.html', {'form':form, 'links':items})

# Will use in future, for now we're using a userless approach
def register(request):
    state = 'unregistered'
    error_message = ''

    if request.method == 'POST':
        if not re.match(r'.+@\w+\.\w+', request.POST['email']):
            state = 'error'
            error_message = 'Email address not valid.'
            print request.POST['email']

        elif len(request.POST['password']) < 4:
            state = 'error'
            error_message = 'Password to short'
            print 'laa'

        else:
            new_user = FeedUser(email=request.POST['email'])
            new_user.set_password(request.POST['password'])
            new_user.save()
            print new_user.email
            new_user.send_confirmation_email()

            state = 'registered'


    return render(request, 'digest/register.html', 
        {'state':state, 'error_message':error_message} )