"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase


class FeedTestCase(TestCase):
    def test_feed_creation(self):
        import datetime
        from digest.models import Feed
        from django.contrib.auth.models import User

        u = User.objects.create_user('sean','therothinator@gmail.com','testing')

        f = Feed()
        f.owner = u
        f.title = 'reddit'
        f.url = 'http://reddit.com'
        f.set_interval_days(7)  # Email every 7 days
        f.next_email = datetime.datetime.now() + datetime.timedelta(seconds=f.email_interval)
        f.save()