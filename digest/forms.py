from django import forms

class EditFeedForm(forms.Form):
    email = forms.EmailField(label="Your email")
    title = forms.CharField()
    url = forms.URLField(label="URL")
    starting = forms.DateField(label="Date of next digest email", help_text="(mm/dd/yyyy) or (yyyy/mm/dd)")
    days_interval = forms.IntegerField(label="How many days between each digest?")
    max_amount = forms.IntegerField(label="Maximum number of items per email")
    enabled = forms.BooleanField(required=False)
