from datetime import datetime, timedelta
from random import randint, choice
import urllib2
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.core.mail import send_mail
from django.conf import settings
from django.forms import ModelForm
from lxml.html import fromstring

EMAIL_SUBJECT = settings.REG_EMAIL_TITLE
EMAIL_MESSAGE = settings.REG_EMAIL_MESSAGE
EMAIL_SENDER = settings.REG_EMAIL_SENDER

CHAR_CHOICES = [chr(derp) for derp in range(97,123) + range(65,91)]  # Lower and uppercase

# Not using now, but let's keep for the future
class FeedUser(AbstractBaseUser):
    email = models.EmailField(max_length=75, unique=True, db_index=True)
    USERNAME_FIELD = 'email'

    verified = models.BooleanField(default=False)
    verification_token = models.CharField(max_length=50,
        default=lambda:''.join([chr(randint(97,122)) for k in range(49)]))

    last_email_sent = models.DateTimeField(auto_now_add=True, db_index=True)

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.get_full_name()

    def send_confirmation_email(self, host='digest.seanroth.com'):
        message = EMAIL_MESSAGE.replace('{host}', host)\
            .replace('{regtoken}', self.verification_token)

        send_mail(EMAIL_SUBJECT, message, EMAIL_SENDER, (self.email,))

class Feed(models.Model):
    title = models.TextField(blank=True)
    url = models.URLField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    last_crawled = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    next_email = models.DateTimeField(blank=True, null=True,
                                      default=lambda: datetime.now() + timedelta(days=1))
    email_interval = models.IntegerField(null=True)  # Time between emails in seconds
    email = models.EmailField()
    #owner = models.ForeignKey(FeedUser)
    xpaths = models.TextField()
    max_amount = models.IntegerField(default=10)
    enabled = models.BooleanField(default=True)
    # Technically not unique, just random
    unique_token = models.CharField(max_length=50, blank=True)

    def get(self, key):
        return getattr(self, key)

    def get_parsed(self):
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', ' Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0')]
        response = opener.open(self.url)
        parsed = fromstring(response.read())
        response.close()
        return parsed

    def __unicode__(self):
        if self.title:
            return self.title
        else:
            return self.url

    def get_absolute_url(self):
        return '/edit_feed/%d/%s/' % (self.pk, self.unique_token)

    def refresh_token(self):
        # Creates string of random upper and lowercase letters
        # For some reason choice() is extremely efficient: this code is 2x
        # faster than the original lowercase implementation.
        # Originally I had choice([randint(97,122),randint(65,90)]),
        # which turned out to be more than 2x SLOWER than original
        #
        # ==Original Lowercase Only==
        # >>> timeit("''.join([chr(randint(97,122)) for k in range(49)])", number=100000, setup="from random import randint, choice")
        # 5.713801860809326
        #
        # ==First try Mixed Case==
        # >>> timeit("''.join([chr(choice([randint(97,122),randint(65,90)])) for k in range(49)])", number=100000, setup="from random import randint, choice")
        # 12.872645854949951
        #
        # ==Good===
        # >>> timeit("''.join([chr(choice(choices)) for k in range(49)])", number=100000, setup="from random import randint, choice;choices=range(97,123)+range(65,91)")
        # 2.4448201656341553
        #
        # ==Great==
        # >>> timeit("''.join([choice(choices) for k in range(49)])", number=100000, setup="from random import randint, choice;choices=[chr(derp) for derp in range(65,91)+range(97,123)]")
        # 2.1216399669647217

        self.unique_token = ''.join([choice(CHAR_CHOICES) for k in range(49)])
        return self.unique_token

    def set_interval_days(self, days):
        self.email_interval = days * 86400

    def get_interval_days(self):
        if self.email_interval:
            return self.email_interval / 86400
        else:
            return None

    def save(self, *args, **kwargs):
        if self.pk is None:  # Happens when this is a new feed
            self.refresh_token()
        super(Feed, self).save(*args, **kwargs)


class FeedItem(models.Model):
    title = models.TextField()
    url = models.URLField(max_length=500)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    feed = models.ForeignKey(Feed)
    xpath = models.TextField()
    score = models.IntegerField(default=0)  # By default is length of time on website, but can be score for
                                   # sites like reddit, news.ycombinator.com, etc.
    last_active = models.DateTimeField(default=datetime.now())  # As far as we know, is this item still on the website?
    #dead = models.BooleanField(default=False)


    def __unicode__(self):
        return self.title